using GeometricShapes.Interface;
using GeometricShapes.Model;




namespace GeometricShapes
{
    public partial class FrmShapes : Form
    {
        List<IShapesCalculator> figuresList = new List<IShapesCalculator>();
        BindingSource bs = new();

        public FrmShapes()
        {
            InitializeComponent();
            bs.DataSource = figuresList;



        }

        void AddFigure(IShapesCalculator figure)
        {

            figuresList.Add(figure);
            lbShapes.DataSource = bs;
            bs.ResetBindings(false);
            labelCount.Text = "There are " + figuresList.Count + " figures in the list.";
            Clean_fields();


        }

        private Triangle CreateTriangle()
        {
            double base1 = Convert.ToDouble(txtBase.Text);
            double side1 = Convert.ToDouble(txtSide1.Text);
            double side2 = Convert.ToDouble(txtSide2.Text);
            Triangle triangle = new(side1, side2, base1);
            return triangle;

        }

        private Circle CreateCircle()
        {
            double radio = Convert.ToDouble(txtRadio.Text);
            Circle circle = new(radio);
            return circle;
        }

        private Square CreateSquare()
        {
            double side = Convert.ToDouble(txtSide.Text);
            Square square = new(side);
            return square;
        }


        private void BtnSquare_Click(object sender, EventArgs e)
        {
            Square square = CreateSquare();
            AddFigure(square);

        }

        private void BtnTriangle_Click(object sender, EventArgs e)
        {
            Triangle triangle = CreateTriangle();
            AddFigure(triangle);
        }

        private void BtnCircle_Click(object sender, EventArgs e)
        {
            Circle circle = CreateCircle();
            AddFigure(circle);
        }

        private void Clean_fields()
        {
            txtSide.Text = "";
            txtSide1.Text = "";
            txtSide2.Text = "";
            txtRadio.Text = "";
            txtBase.Text = "";
        }

        
    }
}