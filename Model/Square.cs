﻿using GeometricShapes.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeometricShapes.Model
{
    internal class Square : IShapesCalculator
    {
        double side;

        public Square(double side)
        {
            this.side = side;
        }

        public double CalculateArea()
        {
            return 4 * side;
        }

        public double CalculatePerimeter()
        {
            return side * side;
        }

        public string ShowData()
        {
            return ToString();
        }

        public override string ToString()
        {
            return "Square. Perimeter: " + CalculatePerimeter() + ". Area: " + CalculateArea(); ;
        }

    }
}
