﻿using GeometricShapes.Interface;
using System;
using System.Buffers.Text;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeometricShapes.Model
{
    internal class Triangle : IShapesCalculator
    {
        double side1;
        double side2;
        double base1;

        public Triangle(double side1, double side2,double base1)
        {
            this.side1 = side1;
            this.side2 = side2;
            this.base1 = base1;
        }

        public double CalculateArea()
        {
            return Math.Sqrt(calculate_semiperimeter() * (calculate_semiperimeter() - base1) * (calculate_semiperimeter() - side1) * (calculate_semiperimeter() - side2));
        }

        public double CalculatePerimeter()
        {
            return base1 + side1 + side2;
        }

        public double calculate_semiperimeter()
        {
            return (0.5 * base1) + side1 + side2;
        }

        public string ShowData()
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return "Triangle. Perimeter: " + CalculatePerimeter()+". Area: " + CalculateArea();
        }

    }
}
