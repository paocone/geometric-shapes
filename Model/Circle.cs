﻿using GeometricShapes.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeometricShapes.Model
{
    internal class Circle : IShapesCalculator
    {
        double radio;

        public Circle (double radio)
        {
            this.radio = radio;
        }

        public double CalculateArea()
        {
            return this.radio * 2 * Math.PI;
        }

        public double CalculatePerimeter()
        {
            return this.radio * this.radio * Math.PI;
        }


        public override string ToString()
        {
            return "Circle. Perimeter: " + CalculatePerimeter() + ". Area: " + CalculateArea(); ;
            
        }

    }
}
