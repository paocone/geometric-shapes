﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeometricShapes.Interface
{
    internal interface IShapesCalculator
    {
        double CalculateArea();
        double CalculatePerimeter();
    }
}
