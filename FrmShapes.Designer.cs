﻿namespace GeometricShapes
{
    partial class FrmShapes
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            triangle = new Panel();
            btnTriangle = new Button();
            txtSide1 = new TextBox();
            label8 = new Label();
            txtBase = new TextBox();
            label7 = new Label();
            txtSide2 = new TextBox();
            label6 = new Label();
            label3 = new Label();
            pnlsquare = new Panel();
            btnSquare = new Button();
            txtSide = new TextBox();
            label5 = new Label();
            label2 = new Label();
            label1 = new Label();
            circle = new Panel();
            btnCircle = new Button();
            txtRadio = new TextBox();
            label9 = new Label();
            label4 = new Label();
            lbShapes = new ListBox();
            labelCount = new Label();
            triangle.SuspendLayout();
            pnlsquare.SuspendLayout();
            circle.SuspendLayout();
            SuspendLayout();
            // 
            // triangle
            // 
            triangle.BackColor = Color.BurlyWood;
            triangle.Controls.Add(btnTriangle);
            triangle.Controls.Add(txtSide1);
            triangle.Controls.Add(label8);
            triangle.Controls.Add(txtBase);
            triangle.Controls.Add(label7);
            triangle.Controls.Add(txtSide2);
            triangle.Controls.Add(label6);
            triangle.Controls.Add(label3);
            triangle.Location = new Point(281, 31);
            triangle.Name = "triangle";
            triangle.Size = new Size(250, 250);
            triangle.TabIndex = 1;
            // 
            // btnTriangle
            // 
            btnTriangle.BackColor = Color.Gold;
            btnTriangle.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnTriangle.Location = new Point(85, 196);
            btnTriangle.Name = "btnTriangle";
            btnTriangle.Size = new Size(75, 33);
            btnTriangle.TabIndex = 11;
            btnTriangle.Text = "CREATE";
            btnTriangle.UseVisualStyleBackColor = false;
            btnTriangle.Click += BtnTriangle_Click;
            // 
            // txtSide1
            // 
            txtSide1.Location = new Point(99, 100);
            txtSide1.Name = "txtSide1";
            txtSide1.Size = new Size(100, 23);
            txtSide1.TabIndex = 10;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.BackColor = Color.SeaShell;
            label8.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label8.Location = new Point(45, 99);
            label8.Name = "label8";
            label8.Size = new Size(40, 21);
            label8.TabIndex = 9;
            label8.Text = "Side";
            // 
            // txtBase
            // 
            txtBase.Location = new Point(98, 163);
            txtBase.Name = "txtBase";
            txtBase.Size = new Size(100, 23);
            txtBase.TabIndex = 8;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.BackColor = Color.SeaShell;
            label7.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label7.Location = new Point(47, 163);
            label7.Name = "label7";
            label7.Size = new Size(42, 21);
            label7.TabIndex = 7;
            label7.Text = "Base";
            // 
            // txtSide2
            // 
            txtSide2.Location = new Point(98, 131);
            txtSide2.Name = "txtSide2";
            txtSide2.Size = new Size(100, 23);
            txtSide2.TabIndex = 6;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.BackColor = Color.SeaShell;
            label6.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label6.Location = new Point(46, 130);
            label6.Name = "label6";
            label6.Size = new Size(40, 21);
            label6.TabIndex = 5;
            label6.Text = "Side";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.BackColor = Color.SeaShell;
            label3.Font = new Font("Segoe UI", 20.25F, FontStyle.Regular, GraphicsUnit.Point);
            label3.Location = new Point(59, 27);
            label3.Name = "label3";
            label3.Size = new Size(137, 37);
            label3.TabIndex = 2;
            label3.Text = "TRIANGLE";
            // 
            // pnlsquare
            // 
            pnlsquare.BackColor = Color.BurlyWood;
            pnlsquare.Controls.Add(btnSquare);
            pnlsquare.Controls.Add(txtSide);
            pnlsquare.Controls.Add(label5);
            pnlsquare.Controls.Add(label2);
            pnlsquare.Controls.Add(label1);
            pnlsquare.Location = new Point(32, 31);
            pnlsquare.Name = "pnlsquare";
            pnlsquare.Size = new Size(250, 250);
            pnlsquare.TabIndex = 2;
            // 
            // btnSquare
            // 
            btnSquare.BackColor = Color.Gold;
            btnSquare.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnSquare.Location = new Point(91, 196);
            btnSquare.Name = "btnSquare";
            btnSquare.Size = new Size(75, 33);
            btnSquare.TabIndex = 5;
            btnSquare.Text = "CREATE";
            btnSquare.UseVisualStyleBackColor = false;
            btnSquare.Click += BtnSquare_Click;
            // 
            // txtSide
            // 
            txtSide.Location = new Point(95, 96);
            txtSide.Name = "txtSide";
            txtSide.Size = new Size(100, 23);
            txtSide.TabIndex = 4;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.BackColor = Color.SeaShell;
            label5.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label5.Location = new Point(44, 95);
            label5.Name = "label5";
            label5.Size = new Size(40, 21);
            label5.TabIndex = 3;
            label5.Text = "Side";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.BackColor = Color.SeaShell;
            label2.Font = new Font("Segoe UI", 20.25F, FontStyle.Regular, GraphicsUnit.Point);
            label2.Location = new Point(66, 27);
            label2.Name = "label2";
            label2.Size = new Size(116, 37);
            label2.TabIndex = 1;
            label2.Text = "SQUARE";
            // 
            // label1
            // 
            label1.Location = new Point(29, 0);
            label1.Name = "label1";
            label1.Size = new Size(100, 23);
            label1.TabIndex = 0;
            // 
            // circle
            // 
            circle.BackColor = Color.BurlyWood;
            circle.Controls.Add(btnCircle);
            circle.Controls.Add(txtRadio);
            circle.Controls.Add(label9);
            circle.Controls.Add(label4);
            circle.Location = new Point(530, 31);
            circle.Name = "circle";
            circle.Size = new Size(250, 250);
            circle.TabIndex = 3;
            // 
            // btnCircle
            // 
            btnCircle.BackColor = Color.Gold;
            btnCircle.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnCircle.Location = new Point(94, 195);
            btnCircle.Name = "btnCircle";
            btnCircle.Size = new Size(75, 33);
            btnCircle.TabIndex = 7;
            btnCircle.Text = "CREATE";
            btnCircle.UseVisualStyleBackColor = false;
            btnCircle.Click += BtnCircle_Click;
            // 
            // txtRadio
            // 
            txtRadio.Location = new Point(105, 102);
            txtRadio.Name = "txtRadio";
            txtRadio.Size = new Size(100, 23);
            txtRadio.TabIndex = 6;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.BackColor = Color.SeaShell;
            label9.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label9.Location = new Point(46, 101);
            label9.Name = "label9";
            label9.Size = new Size(50, 21);
            label9.TabIndex = 5;
            label9.Text = "Radio";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.BackColor = Color.SeaShell;
            label4.Font = new Font("Segoe UI", 20.25F, FontStyle.Regular, GraphicsUnit.Point);
            label4.Location = new Point(77, 28);
            label4.Name = "label4";
            label4.Size = new Size(101, 37);
            label4.TabIndex = 2;
            label4.Text = "CIRCLE";
            // 
            // lbShapes
            // 
            lbShapes.FormattingEnabled = true;
            lbShapes.ItemHeight = 15;
            lbShapes.Location = new Point(32, 300);
            lbShapes.Name = "lbShapes";
            lbShapes.Size = new Size(748, 124);
            lbShapes.TabIndex = 4;
            // 
            // labelCount
            // 
            labelCount.AutoSize = true;
            labelCount.BackColor = Color.SeaShell;
            labelCount.Location = new Point(32, 282);
            labelCount.Name = "labelCount";
            labelCount.Size = new Size(49, 15);
            labelCount.TabIndex = 6;
            labelCount.Text = "Records";
            // 
            // FrmShapes
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            AutoSize = true;
            BackColor = Color.Peru;
            ClientSize = new Size(804, 461);
            Controls.Add(labelCount);
            Controls.Add(lbShapes);
            Controls.Add(circle);
            Controls.Add(pnlsquare);
            Controls.Add(triangle);
            FormBorderStyle = FormBorderStyle.Fixed3D;
            MaximizeBox = false;
            Name = "FrmShapes";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Shape Machine";
            triangle.ResumeLayout(false);
            triangle.PerformLayout();
            pnlsquare.ResumeLayout(false);
            pnlsquare.PerformLayout();
            circle.ResumeLayout(false);
            circle.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private Panel triangle;
        private Panel pnlsquare;
        private Label label1;
        private Panel circle;
        private TextBox txtSide1;
        private Label label8;
        private TextBox txtBase;
        private Label label7;
        private TextBox txtSide2;
        private Label label6;
        private Label label3;
        private TextBox txtSide;
        private Label label5;
        private Label label2;
        private TextBox txtRadio;
        private Label label9;
        private Label label4;
        private ListBox lbShapes;
        private Button btnTriangle;
        private Button btnSquare;
        private Button btnCircle;
        private Label labelCount;
    }
}